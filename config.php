<?php
//DB
$config['db_name'] = 'apklausa';
$config['db_user'] = 'root';
$config['db_password'] = '';
$config['db_host'] = 'localhost';

//Birthdate
$config['min_age'] = 7;
$config['max_age'] = 100;

//Max photo file size (in KB)
$config['photo_max_size'] = '500';

//upload dir
$config['upload_dir'] = 'photos';