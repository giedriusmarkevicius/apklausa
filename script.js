var height = window.innerHeight;
var page = $('#page').val();

if (page >1) { //set current page
	var current_height = (page-1) * height;
	$('form').css('margin-top', '-' + current_height + 'px');
}

$(function() {
	
	$('.continue').click(function() {
		next();
	});
	
	$('.submit').click(function() {
		$('form').submit();
	});
	
	$('#year, #month').change(function() {
		var numberOfDays = new Date($('#year').val(), $('#month').val(), 0).getDate();  //get number of days
		var selectedDay = $('#day').val(); //we need to keep a selected day
		var days = '<option value="0">-Diena-</option>';
		
		for (i = 1; i <= numberOfDays; i++) {
			
			days += '<option value="'+i+'"';
			
			if (i == selectedDay) {
				days += ' selected="selected"'; //save selected day
			}
			
			days += '>'+i+'</option>';
		}
		
		$('#day').html(days); //update number of days
	});
	
	$('.no_languages').change(function() {
		if ($(this).is(':checked')) {
			$('.language').prop("checked", false);
			$('.language').attr("disabled", true);
		}
		else {
			$('.language').attr("disabled", false);
		}
		//
	});
	
});

function next() {
	$.post('index.php', $('form').serialize(), function(data) {
		if (data == '2') {
			location.reload();
		} else if (data == '1') {
			$('#page').val(Number($('#page').val())+1);
			$('form').animate({
				marginTop: "-=" + height + "px",
			}, 200);
		} else {
			$('#error'+$('#page').val()).html(data);
		}
	});
	
	
}
