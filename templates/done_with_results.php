<?php
if (!isset($config)) {
	die(); //no direct access allowed
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="robots" content="noindex, nofollow">
<title>Apklausa</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
	<main>
		<section>
			<h1>Jūsų anketa</h1>
			<table>
				<tr>
					<td rowspan="4">
						<?php if ($user['photo'] != ''): ?>
						<img src="photos/<?php echo $user['photo']; ?>" alt="<?php echo $user['name']; ?>">
						<?php endif; ?>
					</td>
					<td><strong>Vardas:</strong> <?php echo $user['name']; ?></td>
				</tr>
				<tr>
					<td><strong>Gimimo data:</strong> <?php echo $user['birthdate']; ?></td>
				</tr>
				<tr>
					<td><strong>Lytis:</strong> <?php echo ($user['sex'] == 1) ? 'Vyras' : 'Moteris'; ?></td>
				</tr>
				<tr>
					<td>
						<strong>Programavimo kalbos:</strong>
						<ul>
						<?php foreach ($languages as $language): ?>
						<?php if ($language['user_ip']) echo '<li>'.$language['name'].'</li>'; ?>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>
			</table>
			
			
		</section>
	</main>
</body>
</html>