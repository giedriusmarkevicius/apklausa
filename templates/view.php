<?php
if (!isset($config)) {
	die(); //no direct access allowed
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="robots" content="noindex, nofollow">
<title>Apklausa</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
	<main>
		<form name="apklausa" action="index.php" method="post" enctype="multipart/form-data"> 
		<section>
			<h1>1. Koks Jūsų vardas?</h1>
			<input type="text" name="name" value="<?php echo $user['name']; ?>">
			<a href="javascript:;" title="tęsti" class="continue button">Tęsti</a>
			<p id="error1" class="error"></p>
		</section>
		<section>
			<h1>2. Jūsų gimimo data</h1>
			<select name="year" id="year">
				<option value="0">-Metai-</option>
				<?php
					for ($i = date('Y', strtotime("-".$config['min_age']." years")); $i >= date('Y', strtotime("-".$config['max_age']." years")); $i--) {
						echo '<option value="'.$i.'"';
						if (substr($user['birthdate'], 0, 4) == $i) { echo ' selected="selected"'; }
						echo '>'.$i.'</option>';
					}
				?>
			</select>
			<select name="month" id="month">
				<option value="0">-Mėnuo-</option>
				<option value="01"<?php if (substr($user['birthdate'], 5, 2) == '01') { echo ' selected="selected"'; } ?>>Sausis</option>
				<option value="02"<?php if (substr($user['birthdate'], 5, 2) == '02') { echo ' selected="selected"'; } ?>>Vasaris</option>
				<option value="03"<?php if (substr($user['birthdate'], 5, 2) == '03') { echo ' selected="selected"'; } ?>>Kovas</option>
				<option value="04"<?php if (substr($user['birthdate'], 5, 2) == '04') { echo ' selected="selected"'; } ?>>Balandis</option>
				<option value="05"<?php if (substr($user['birthdate'], 5, 2) == '05') { echo ' selected="selected"'; } ?>>Gegužė</option>
				<option value="06"<?php if (substr($user['birthdate'], 5, 2) == '06') { echo ' selected="selected"'; } ?>>Birželis</option>
				<option value="07"<?php if (substr($user['birthdate'], 5, 2) == '07') { echo ' selected="selected"'; } ?>>Liepa</option>
				<option value="08"<?php if (substr($user['birthdate'], 5, 2) == '08') { echo ' selected="selected"'; } ?>>Rugpjūtis</option>
				<option value="09"<?php if (substr($user['birthdate'], 5, 2) == '09') { echo ' selected="selected"'; } ?>>Rugsėjis</option>
				<option value="10"<?php if (substr($user['birthdate'], 5, 2) == '10') { echo ' selected="selected"'; } ?>>Spalis</option>
				<option value="11"<?php if (substr($user['birthdate'], 5, 2) == '11') { echo ' selected="selected"'; } ?>>Lapkritis</option>
				<option value="12"<?php if (substr($user['birthdate'], 5, 2) == '12') { echo ' selected="selected"'; } ?>>Gruodis</option>
			</select>
			<select name="day" id="day">
				<option value="0">-Diena-</option>
				<?php
					$month = ((int) substr($user['birthdate'], 5, 2) != 0) ? (int) substr($user['birthdate'], 5, 2) : 1;
					$year = ((int) substr($user['birthdate'], 0, 4) != 0) ? (int) substr($user['birthdate'], 0, 4) : 2018;
					for ($i = 1; $i <= cal_days_in_month(CAL_GREGORIAN, $month, $year); $i++) {
						echo '<option value="'.$i.'"';
						if ((int) substr($user['birthdate'], -2) == $i) { echo ' selected="selected"'; }
						echo '>'.$i.'</option>';
					}
				?>
			</select>
			<a href="javascript:;" title="tęsti" class="continue button">Tęsti</a>
			<p id="error2" class="error"></p>
		</section>
		<section>
			<h1>3. Lytis</h1>
			<label><input type="radio" name="sex" value="1"<?php if ($user['sex'] == 1) echo ' checked="checked"'; ?>> Vyras</label>
			<label><input type="radio" name="sex" value="2"<?php if ($user['sex'] == 2) echo ' checked="checked"'; ?>> Moteris</label>
			<a href="javascript:;" title="tęsti" class="continue button">Tęsti</a>
			<p id="error3" class="error"></p>
		</section>
		<section>
			<h1>4. Ar domitės programavimu?</h1>
			<label><input type="radio" name="programming" value="1"<?php if ($user['programming'] == 1) echo ' checked="checked"'; ?>> Taip</label>
			<label><input type="radio" name="programming" value="2"> Ne</label>
			<a href="javascript:;" title="tęsti" class="continue button">Tęsti</a>
			<p id="error4" class="error"></p>
		</section>
		<section>
			<h1>5. Kokias programavimo kalbas mokate?</h1>
			<div class="languages">
				<?php foreach ($languages as $language): ?>
				<label><input type="checkbox" name="languages[<?php echo $language['id']; ?>]" value="1" class="language"<?php if ($language['user_ip']) echo ' checked="checked"'; ?>> <?php echo $language['name']; ?></label>
				<?php endforeach; ?>
				<label><input type="checkbox" name="languages[0]" value="1" class="no_languages"> Nemoku nei vienos</label>
				<a href="javascript:;" title="tęsti" class="continue button">Tęsti</a>
				<p id="error5" class="error"></p>
			</div>
			
		</section>
		<section>
			<h1>6. Prašome patalpinti savo nuotrauką:</h1>
			<input type="file" name="photo">
			<a href="javascript:;" title="tęsti" class="submit button">Tęsti</a>
			<p id="error6" class="error"><?php echo $error; ?></p>
		</section>
		<input type="hidden" name="page" value="<?php echo $user['page']; ?>" id="page">
	</form>
	</main>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="script.js"></script>
</body>
</html>