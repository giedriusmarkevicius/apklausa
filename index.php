<?php
require_once 'config.php';

//database
$db = new PDO('mysql:dbname='.$config['db_name'].';host='.$config['db_host'].';charset=utf8', $config['db_user'], $config['db_password']);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //show errors

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

if ($ip == '::1') { //localhost
    $ip = '127.0.0.1'; 
}

$ip = ip2long($ip); //convert to integer

//ajax
if (isset($_POST['page']) && $_POST['page'] < 6) {
    $sex = (isset($_POST['sex'])) ? (int) $_POST['sex'] : 0;
    $programming = (isset($_POST['programming'])) ? (int) $_POST['programming'] : 0;
    $name = (isset($_POST['name'])) ? $_POST['name'] : 0;
    $page = (isset($_POST['page'])) ? $_POST['page'] : 1;
    
    //format birthdate
    $year = (int) $_POST['year'];
    $month = (int) $_POST['month'];
    $day = ((int) $_POST['day'] > 9) ? (int) $_POST['day'] : '0' . (int) $_POST['day'];
    
    if ($year != 0 && $month != 0 && $day != 0) {
        $birthdate = $year.'-'. $month.'-'.$day ;
    } else { 
		$birthdate = null;
	}
        
    //no programming languages
    $noLanguage = 0;
    
    if (isset($_POST['languages'][0]) && $_POST['languages'][0] == 1) {
        $noLanguage = 1;
    }
	    
    if ($page == 1 && $name == '') {
        echo "Prašome įrašyti vardą.";
        die();
    }
    
    if ($page == 2 && $birthdate == null) {
        echo "Prašome nurodyti gimimo datą.";
        die();
    }
    
    if ($page == 3 && $sex === 0) {
        echo "Prašome nurodyti lytį.";
        die();
    }
    
    if ($page == 4 && $programming === 0) {
        echo "Prašome atsakyti ar domities programavimu.";
        die();
    }
    
    if ($page == 5 && !isset($_POST['languages'])) {
        echo "Prašome nurodyti kurias programavimo kalbas mokate";
        die();
    }
        
    $st = $db->prepare("UPDATE users SET name = :name, sex = :sex, birthdate = :birthdate, programming = :programming, no_language = :no_language, page = :page WHERE ip = :ip"); //update user
    $st->execute(
        [
            'name' => $name,
            'sex' => $sex,
            'birthdate' => $birthdate,
            'programming' => $programming,
            'no_language' => $noLanguage,
            'page' => $page,
            'ip' => $ip
        ]
    );
    
    //insert languages
    if (isset($_POST['languages']) && !isset($_POST['languages'][0])) {
        $query = "INSERT IGNORE INTO users_langauges (user_ip, language_id) VALUES ";
        
        foreach ($_POST['languages'] as $lang => $check) {
            $query .= "('".$ip."','". (int) $lang ."'),";
        }
        
        $query = substr($query,0,-1);
        $st = $db->prepare($query);
        $st->execute();
    }

    if ($programming == 2 || $noLanguage == 1 || $page > 6) {
        die('2'); //refresh
    }
    
    die('1');
} //end ajax

//last step upload file
$error = $filename = '';

if (isset($_POST['page']) && $_POST['page'] == 6) {
    
    if (isset($_FILES["photo"]) && $_FILES["photo"]["tmp_name"] != '') {
        $image = getimagesize($_FILES["photo"]["tmp_name"]);
        
        if(!$image) {
            $error = "Failas privalo būti paveiksliukas";
        }
        
        if ($_FILES["photo"]["size"] > $config['photo_max_size']*1000) {
            $error = "Failas privalo būti ne didesnis nei ".$config['photo_max_size']."KB";
        }
        
        if ($error == '') {
            $ext = strtolower(pathinfo($_FILES["photo"]["name"],PATHINFO_EXTENSION));
            $filename = uniqid().'.'.$ext;
            
            if (move_uploaded_file($_FILES["photo"]["tmp_name"], $config['upload_dir'].'/'.$filename)) {
                
            } else {
                $error = "Įvyko klaida, failas neįkeltas";
            }
        }
    }
	
	if ($error == '') {
		$st = $db->prepare("UPDATE users SET page = 7, photo = :photo WHERE ip = :ip"); //update user page
		$st->execute(['ip' => $ip, 'photo' => $filename]);
		header("Refresh:0"); //Prevent re-posting
		die();
	}
	else {
		$st = $db->prepare("UPDATE users SET page = 6 WHERE ip = :ip"); //update user page
		$st->execute(['ip' => $ip]);
	}
}
//end last step

//get user
$st = $db->prepare("SELECT * from users WHERE ip = :ip");
$st->execute(['ip' => $ip]);
$user = $st->fetch(PDO::FETCH_ASSOC); 

if (!$user) {
    $st = $db->prepare("INSERT INTO users (ip) VALUES (:ip)"); 
    $st->execute(['ip' => $ip]);
    $user = [ //default values
        'ip' => $ip,
        'name' => '',
        'sex' => 0,
        'birthdate' => '',
        'programming' => 0,
        'no_language' => 0,
        'photo' => '',
        'page' => '1'
    ];
}

//if user not interested in programming or does not know any programming language then show 'done_without_results.php' template
if ($user['programming'] == 2 || $user['no_language'] == 1) { 
    require_once 'templates/done_without_results.php';
    die();
}

//get all programming languages
$st = $db->prepare("SELECT u.user_ip, l.id, l.name from languages l LEFT JOIN users_langauges u ON (u.user_ip = :ip AND u.language_id = l.id)"); 
$st->execute(['ip' => $ip]);
$languages = $st->fetchAll(PDO::FETCH_ASSOC);

//if user completed poll then show results
if ($user['page'] > 6) {
    require_once 'templates/done_with_results.php';
    die();
}

//show information
require_once 'templates/view.php';
die();

?>